#from SQL to what ever elastic understands
#show all fields in an index
POST _sql?format=txt  
{  
  "query": """  
describe "poc1-2020.08.05"
  """  
} 

POST _sql?format=txt  
{  
  "query": """  
select Name,"from" from "imdb-2021.01.07"
  """  
} 

#show all possible sql functions to use
POST _sql?format=txt 
{ 
"query":""" 
SHOW FUNCTIONS  
""" 
} 

#find docs where "Hospital_Name": "Hospital_Name"
GET /tst-2020.11.01/_search
{
 "query": { "match": { "Hospital_Name": "Hospital_Name" }  } 
}

#find all documents with the tags "_grokparsefailure"
GET /tst-2020.11.01/_search
{
  "query": {
    "bool" : {
      "must_not" : {
        "term" : {
          "tags" : "_grokparsefailure"
        }
      }
    }
  }
}

#create new index named dev and 
PUT dev-2020.11.01
{
  "mappings" : {
    "properties": {
      "user_id": {
        "type": "keyword"
      }
    }
  }
}
#copy everything from tst to dev index
POST /_reindex
{
  "source": {
    "index": "tst-2020.11.01"
  },
  "dest": {
    "index": "dev-2020.11.01"
  }
}

#check the mapping of an existing index (i.e - what fields does each document in an index have) 
GET /tst-2020.11.01/_mapping

#add field named title of type text to index
PUT /tst-2020.11.01/_mapping
{
  "properties" :{
    "title": {"type":"text"}
  }
}

#what does this do? if it adds a new document to an index then i couldn't find it after in discover . only in search
PUT /tst-2020.11.01/_doc/1
{
  "host":"handJob",
  "date_":"01/11/2020",
  "last":"SharedSchedule",
  "time_":"19:28:00",
  "hospital_name":"5"
}

#update the field hospital_name in all documents where id_hospital is 11101 of index tst-2020.11.02
POST tst-2020.11.02/_update_by_query
{
    "script" : {
      "source": "ctx._source.Hospital_Name='shr';",
      "lang": "painless"  
    },
    "query": {
        "term" : {
            "id_hospital": "11101"
        }
    }
}
